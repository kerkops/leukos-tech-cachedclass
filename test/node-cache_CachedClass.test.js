// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const factory = require("../index");

// Engine per la cache (package npm) da utilizzare
const CACHE_ENGINE = "node-cache";

//const tested = factory()
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** 
 * Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  
 * 
 * Per attivare tale modalità utilizzare da command line:
 * 
 * (es. DEBUG_PROP = `"DEBUG"`)
 * 
 * `npm run script <scriptName> env `**`DEBUG=true`**
 * */
const env_DEBUG_PROP = "DEBUG";

/** 
 * Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`.
 * */
const DEBUG_ENABLED = (['1', 'true'].includes(process.env.DEBUG)) ? true : false
console.log(`DEBUG_ENABLED: ${DEBUG_ENABLED}`)



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const expect = require('chai').expect;
const jsutils = require('leukos-tech-jsutils');
const graphicHeader = jsutils.tests.getHeader;


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
// const aVar = require('');
// const aVar = require('');
// const aVar = require('');


describe (`${graphicHeader(`Leukos-Tech-CachedClass( engine="${CACHE_ENGINE}" )`, 0)}`, function() {

    // Variabili con Scope locale a questa unità vanno qui
    let mainHeader = `CachedClass("${CACHE_ENGINE}")`;
    
    let _set_declaration = `${mainHeader}.cacheSet(key, value): Boolean`;
    let _get_declaration = `${mainHeader}.cacheGet(key): Any`;
    let _del_declaration = `${mainHeader}.cacheDel(key): Boolean`;
    
    let _set_args_declaration = `${mainHeader}.cacheSet_args(key, args, value): Boolean`;
    let _get_args_declaration = `${mainHeader}.cacheGet_args(key, args): Any`;
    let _del_args_declaration = `${mainHeader}.cacheDel_args(key, args): Boolean`;
    
    let CLASS_OBTAINED=false;
    let tested;
    
    let testDate = new Date();
    let testSymbol = Symbol();
    let testArgs = [1, 'hello', false, testDate];

    /** dati per i test (la chiave equivale al type del dato)*/
    const sources = {
        "string": "Stringa di test @ leukos-tech-cachedclass",
        "number": 12345678,
        "boolean": true,
        "array": ["HI", 34, false, {hello:"world"}],
        "object": {array: [1,2,3,4], string: "Hello from `object`", number: 8976, boolean:false, undef:undefined, nullo:null},
        // "date": testDate,
        // "symbol": testSymbol
        // "null": null,
        // "undefined": undefined,
        // "Symbol": new Symbol("testSymbol")
    }

    // ~~~~~~~~~~~~~~~~ CachedClass.cacheSet() ~~~~~~~~~~~~~~~~~~ //`
    describe(`${graphicHeader(_set_declaration, 1)}`,function(){
        
        // ~~~~~~~~~~~~~~~~ CachedClass.cacheSet() ~~~~~~~~~~~~~~~~~~ //`
        it(`TC001 - Memorizza un valore e restituisce \`true\`, se tale valore è diverso da \`undefined\``, function(){
            let thisHeader = 'TC001';
            let header = `${_set_declaration} - ${thisHeader}`;

            // Skip del test in base ad alcune condizioni
            if (!CLASS_OBTAINED) {
                console.warn(`\t# TEST SKIPPED: Classe da testare (${mainHeader}) non ottenuta correttamente #`);
                this.skip();
            } 
            for (let key of Object.keys(sources)) {
                if (key != 'undefined') {
                    let res = tested.cacheSet(key, sources[key]);
                    if (DEBUG_ENABLED) console.info(`${header} - ${key} - Res: ${res}`);
                    expect(res).equal(true);
                    
                    // expect(src).equal(cfr);
                }
            }
            
        }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
        
        it(`TC002 - Non memorizza e restituisce \`false\`, se l'argomento \`value\` è \`undefined\``, function(){
            let thisHeader = 'TC002'
            let header = `${_set_declaration} - ${thisHeader}`;

            // Skip del test in base ad alcune condizioni
            if (!CLASS_OBTAINED) {
                console.warn(`\t# TEST SKIPPED: Classe da testare (${mainHeader}) non ottenuta correttamente #`);
                this.skip();
            } 
            let res = tested.cacheSet("invalid", undefined);
            if (DEBUG_ENABLED) console.info(`${header} - Res: ${res}`);
            expect(res).equal(false);
            
            
        }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    });

    // ~~~~~~~~~~~~~~~~ CachedClass.cacheGet() ~~~~~~~~~~~~~~~~~~ //`
    describe(`${graphicHeader(_set_args_declaration, 1)}`,function(){
        
        // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
        it(`TC003 - Memorizza un valore e restituisce \`true\`, se tale valore è diverso da \`undefined\``, function(){
            let thisHeader = 'TC003';
            let header = `${_set_args_declaration} - ${thisHeader}`;

            // Skip del test in base ad alcune condizioni
            if (!CLASS_OBTAINED) {
                console.warn(`\t# TEST SKIPPED: Classe da testare (${mainHeader}) non ottenuta correttamente #`);
                this.skip();
            } 
            let res = tested.cacheSet_args("ilNomeDiUnMetodo", [sources["string"], sources['number'], true, [] ], "hello");
            if (DEBUG_ENABLED) console.info(`${header} - Res: ${res}`);
            expect(res).equal(true);
                    
                    // expect(src).equal(cfr);
            
        }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
        
        it(`TC004 - Non memorizza e restituisce \`false\`, se l'argomento \`value\` è \`undefined\``, function(){
            let thisHeader = 'TC004'
            let header = `${_set_args_declaration} - ${thisHeader}`;

            // Skip del test in base ad alcune condizioni
            if (!CLASS_OBTAINED) {
                console.warn(`\t# TEST SKIPPED: Classe da testare (${mainHeader}) non ottenuta correttamente #`);
                this.skip();
            } 
            let res = tested.cacheSet_args("invalid", [1,2,3], undefined);
            if (DEBUG_ENABLED) console.info(`${header} - Res: ${res}`);
            expect(res).equal(false);
            
            
        }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    })

    describe(`${graphicHeader(_get_declaration, 1)}`,function(){
        
        // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
        it(`TC005 - Restituisce il valore salvato`, function(){
            let thisHeader = 'TC005';
            let header = `${_get_declaration} - ${thisHeader}`;
            

            // Skip del test in base ad alcune condizioni
            if (!CLASS_OBTAINED) {
                console.warn(`\t# TEST SKIPPED: Classe da testare (${mainHeader}) non ottenuta correttamente #`);
                this.skip();
            } 
            for (let key of Object.keys(sources)) {
                let isSet = tested.cacheSet(key, sources[key]);
                if (!isSet) {
                    console.warn(`\t TEST SKIPPED: impossibile salvare valore di tipo ${key} (${sources[key]}). `);
                    this.skip();
                };

                let res = tested.cacheGet(key)
                if (DEBUG_ENABLED) console.info(`${header} - ${key} - Res: ${res}`);
                // Comparazione tra oggetti convertiti in stringa
                
                expect(res.toString()).equal(sources[key].toString());
            };
            
        }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
        
        
    });

    describe(`${graphicHeader(_get_args_declaration, 1)}`,function(){
        
        // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
        it(`TC006 - Restituisce il valore salvato`, function(){
            let thisHeader = 'TC006';
            let header = `${_get_args_declaration} - ${thisHeader}`;

            // Skip del test in base ad alcune condizioni
            if (!CLASS_OBTAINED) {
                console.warn(`\t# TEST SKIPPED: Classe da testare (${mainHeader}) non ottenuta correttamente #`);
                this.skip();
            } 
            for (let key of Object.keys(sources)) {
                let value = sources[key];
                let isSet = tested.cacheSet_args(key, testArgs, value);
                if (!isSet) {
                    console.warn(`\t TEST SKIPPED: impossibile salvare valore di tipo ${key} (argomenti ${testArgs}) (${sources[key]}). `);
                    this.skip();
                };
                // if (DEBUG_ENABLED) console.info(`VALORE IMPOSTATO: ${isSet}`)
                // if (DEBUG_ENABLED) console.info(`${header} - key: "${key} - "testArgs: ${testArgs}`);
                // if (DEBUG_ENABLED) console.info(`${header} - valeu: ${value}`);
                
                let res = tested.cacheGet_args(key, testArgs)
                if (DEBUG_ENABLED) console.info(`${header} - ${key} - Res: ${res}`);
                // Comparazione tra oggetti convertiti in stringa
                
                expect(res.toString()).equal(value.toString());
                
                    // expect(src).equal(cfr);
            };
        }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
        
        
    });

    describe(`${graphicHeader(_del_declaration, 1)}`,function(){
        
        // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
        it(`TC007 - Restituisce \`true\' ed elimina il valore se la chiave fornita è valida`, function(){
            let thisHeader = 'TC007';
            let header = `${_del_declaration} - ${thisHeader}`;
            

            // Skip del test in base ad alcune condizioni
            if (!CLASS_OBTAINED) {
                console.warn(`\t# TEST SKIPPED: Classe da testare (${mainHeader}) non ottenuta correttamente #`);
                this.skip();
            } 
            for (let key of Object.keys(sources)) {
                let isSet = tested.cacheSet(key, sources[key]);
                if (!isSet) {
                    console.warn(`\t TEST SKIPPED: impossibile salvare valore di tipo ${key} (${sources[key]}). `);
                    this.skip();
                };

                let value = tested.cacheGet(key)
                if (value === undefined) {
                    console.warn(`\t# TEST SKIPPED: il metodo \`CachedClass.cacheGet(key)\` ha restituito \'undefined\' - Nessun valore associato alla chiave "${key}"#`);
                    this.skip();
                }
                let res = tested.cacheDel(key)
                if (DEBUG_ENABLED) console.info(`${header} - ${key} - Res: ${res}`);
                // Comparazione tra oggetti convertiti in stringa
                
                expect(res).equal(true);
                expect(tested.cacheGet(key)).equal(undefined);
            };
            
        }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
        
        
    });

    describe(`${graphicHeader(_del_args_declaration, 1)}`,function(){
        
        // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
        it(`TC008 - Restituisce \`true\' ed elimina il valore se la combinazione nomeMetodo+argomenti è valida`, function(){
            let thisHeader = 'TC008';
            let header = `${_del_args_declaration} - ${thisHeader}`;

            // Skip del test in base ad alcune condizioni
            if (!CLASS_OBTAINED) {
                console.warn(`\t# TEST SKIPPED: Classe da testare (${mainHeader}) non ottenuta correttamente #`);
                this.skip();
            } 
            for (let key of Object.keys(sources)) {
                let isSet = tested.cacheSet_args(key, testArgs, sources[key]);
                if (!isSet) {
                    console.warn(`\t TEST SKIPPED: impossibile salvare valore di tipo ${key} (argomenti ${testArgs}) (${sources[key]}). `);
                    this.skip();
                };

                let value = tested.cacheGet_args(key, testArgs)

                let res = tested.cacheDel_args(key, testArgs);
                if (DEBUG_ENABLED) console.info(`${header} - ${key} - Res: ${res}`);
                // Comparazione tra oggetti convertiti in stringa
                
                expect(res).equal(true);

                expect(tested.cacheGet_args(key, testArgs)).equal(undefined);

                
                    // expect(src).equal(cfr);
            };
        }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
        
        
    });


 /**
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  *                     IMPOSTAZIONI DEI TEST
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  */
    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    // before( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - Before() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    before( function () {
        try {
            tested = factory(CACHE_ENGINE);
            CLASS_OBTAINED=true;
        } catch (err) {
            err.message = `${mainHeader} - before() fallita - ${err.name} - ${err.message}`;
            console.error(err.message);
            CLASS_OBTAINED=false;
        }
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    // after( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - after() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    afterEach( function () {
        try {
            // Azzera la cache
            tested.cacheFlush();
            // Codice da eseguire prima di TUTTA LA SUITE QUI 
        } catch (err) {
            err.message = `${mainHeader} - after() fallita: Il metodo \`.flush()\` ha generato un errore - ${err.name} - ${err.message}`;
            console.error(err.message);
        }
    });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    // beforeEach( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - BeforeEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - BeforeEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    // afterEach( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - afterEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - afterEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/