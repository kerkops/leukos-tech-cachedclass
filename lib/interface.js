
/**
 * Logga come errore la mancata implementazione di un metodo dell'interfaccia
 * @param {String} mName chiamata al metodo sotto forma di stringa
 */
function notImplemented(mName) {
    console.error(`!!! NOT IMPLEMENTED !!! \`CachedClass.${mName}\` !!! NOT IMPLEMENTED !!!`)
};

/**
 * Interfaccia della classe CachedClass.
 */
class CachedClassInterface {

    /** I metodi da reimplementare si limitano a loggare un reminder */
    _cacheDel() {notImplemented("_del()")};
    _cacheDel_args() {notImplemented("_del_args()")};
    _cacheFlush() {notImplemented("_flush()")};
    _cacheGet() {notImplemented("_get()")};
    _cacheGet_args() {notImplemented("_get_args()")};
    _cacheSet() {notImplemented("_set()")};
    _cacheSet_args() {notImplemented("_set_args()")};

    /**
     * Elimina un elemento dalla cache.
     * @param {String} key Chiave associata all'elemento da eliminare.
     * 
     * @return {Boolean} `true` se l'elemento è stato eliminato (**è stato trovato**).
     */
    cacheDel (key) { return this._cacheDel(key); };


    /**
     * Elimina il valore, memorizzato nella cache, prodotto dalla chiamata al metodo `methodName` con gli argomenti `args`.
     * @param {String} methodName Nome del metodo che ha generato il valore da eliminare.
     * @param {Array} args Array di argomenti utilizzati per la chiamata che ha prodotto il valore.
     * 
     * @return {Boolean} `true` se l'elemento è stato eliminato (**è stato trovato**).
     */
    cacheDel_args(methodName, args) { return this._cacheDel_args(methodName, args); };

    /**
     * Azzera la cache eliminando tutti i valori memorizzati
     */
    cacheFlush() { return this._cacheFlush(); };
    
    /**
     * Legge la cache alla ricerca dell'elemento contrassegnato dalla chiave `key`; se lo trova restituisce il valore, altrimenti `undefined`.
     * @param {String} key Chiave associata all'elemento
     * 
     * @return {Any} Il valore associato alla chiave, se presente; altrimenti *`undefined`*;
     */
    cacheGet(key) { return this._cacheGet(key); };

    /**
     * Restituisce il valore memorizzato nella cache della chiamata a `methodName` con gli argomenti `args`.
     * Se nessun elemento della cache corrisponde a tale combinazione restituisce *`undefined`*.
     * 
     * @param {String} methodName Nome del metodo che ha generato il valore memorizzato.
     * @param {Array} args Array degli argomenti utilizzati per la chiamata al metodo.
     * 
     * @return {Any} Il valore associato alla chiave, se presente; altrimenti `undefined`;
     */
    cacheGet_args(methodName, args) { return this._cacheGet_args(methodName, args); };


    /**
     * Memorizza il valore `value` sotto la chiave `key`.
     * 
     * @param {String} key Chiave da utilizzare per identificare il valore nella cache
     * @param {Any} value Valore da memorizzare. Deve essere diverso da `undefined`. Se uguale, il metodo restiuisce `false` e non memorizza il valore.
     * 
     * @return {Boolean} `true` se il valore è stato memorizzato correttamente.
     */
    cacheSet(key, value) { return this._cacheSet(key, value); };

    /**
     * Memorizza il valore `value` restituito dalla chiamata al metodo `methodName` con gli argomenti `args`.
     * 
     * @param {String} methodName Nome del metodo di cui si vuol salvare il valore restituito.
     * @param {Array} args Array di argomenti utilizzati per la chiamata al metodo.
     * @param {Any} value Valore da memorizzare. Deve essere diverso da `undefined`. Se uguale, il metodo restiuisce `false` e non memorizza il valore.
     *
     * @return {Boolean} `true` se il valore è stato memorizzato correttamente
     */
    cacheSet_args(methodName, args, value) { return this._cacheSet_args(methodName, args, value); };

};

module.exports = CachedClassInterface;