const Interface = require('./interface');
const _cache = require('node-cache');


/**
 * Classe di caching basata sul modulo `node-cache`
 */
class node_cache_CachedClass extends Interface {

    /**
     * Genera l'istanza della classe
     */
    constructor () { 
        super(); 

        /** @property {Object} _cache Oggetto cache */
        this._cache = new _cache()
    };
    
    /**
     * Elimina un elemento dalla cache
     * @param {String} key Chiave associata all'item da eliminare
     * 
     * @return {Boolean} `true` se l'elemento è stato eliminato
     */
    _cacheDel(key) {
        // Il metodo restituisce il numero di elementi eliminati
        let itemsDeleted = this._cache.del(key);
        // Si risolve in `true` se il numero di items eliminati è > 0
        if (itemsDeleted) { 
            return true
        // 0 items eliminati: risolve in `false`
        } else {
            return false
        } 

    };

    /**
     * Elimina un elemento dalla cache, identificandolo tramite un set supplementare di argomenti.
     * 
     * @param {String} key Chiave associata all'elemento da eliminare.
     * @param {Array} args Array di argomenti per la chiamata alla funzione memorizzata.
     *
     *  @return {Boolean} `true` se l'elemento è stato eliminato (*items eliminati > 0*).
     */
    _cacheDel_args(key, args) { return this._cacheDel(_keyArgsString(key, args)) };

    /**
     * Azzera la cache eliminando tutti i valori memorizzati
     */
    _cacheFlush() { this._cache.flushAll()};
    
    /**
     * Legge la cache alla ricerca dell'elemento contrassegnato dalla chiave `key`; se lo trova restituisce il valore, altrimenti `undefined`
     * @param {String} key Chiave associata all'elemento
     * 
     * @return {Any} Il valore associato alla chiave, se presente; altrimenti `undefined`;
     */
    _cacheGet(key) { return this._cache.get(key); };


    /**
     * Legge la cache alla ricerca dell'elemento contrassegnato dalla chiave `key`, identificandolo attraverso un set supplementare di argomenti; se lo trova restituisce il valore associato, altrimenti `undefined`.
     * 
     * @param {String} key Nome del metodo di cui recuperare il valore memorizzato
     * @param {Array} args Array di argomenti della chiamata alla funzione
     * 
     * @return {Any} Il valore associato alla chiave, se presente; altrimenti `undefined`;
     */
    _cacheGet_args(key, args) { 
        // console.warn("CHIAMATA AL METODO _cacheGet_args()")
        // let _key = _keyArgsString(key, args);
        // console.warn(`Chiave complessa: ${_key}`);
        // let value = this._cache.get(_key);
        // console.warn(`Valore restituito: ${value}`);
        // return value;
        return this._cache.get( _keyArgsString(key, args) ); 
    };


    /**
     * Memorizza il valore `value` sotto la chiave `key`.
     * 
     * @param {String} key Chiave da utilizzare per identificare il valore nella cache
     * @param {Any} value Valore da memorizzare. Deve essere diverso da `undefined`. Se uguale, il metodo restiuisce `false` e non memorizza il valore.
     * 
     * @return {Boolean} `true` se il valore è stato memorizzato correttamente.
     */
    _cacheSet(key, value) { 
        if (value != undefined) {
            return this._cache.set(key, value); 
        };
        return false;
    };


    /**
     * Memorizza il valore `value` sotto la chiave `key`, identificandolo attraverso un set supplementare di argomenti.
     * 
     * @param {String} key Chiave da utilizzare per identificare il valore nella cache
     * @param {Array} args Array di argomenti della chiamata alla funzione
     * @param {Any} value Valore da memorizzare. Deve essere diverso da `undefined`. Se uguale, il metodo restiuisce `false` e non memorizza il valore.
     *
     * @return {Boolean} `true` se il valore è stato memorizzato correttamente
     */
    _cacheSet_args(key, args, value) {
        if (value != undefined) {
         
            return this._cache.set(_keyArgsString(key, args), value);
        };
        return false;
    };

};

/**
 * Restituisce la stringa da assegnare come chiave per un metodo con argomenti
 * @param {String} key Nome del metodo di cui ricavare la chiave
 * @param {Array} args Array di argomenti
 */
function _keyArgsString(key, args) {
    try {
        // let complex = `${key}(${Array(args).join(',')})`;
        // console.info("STRINGA key GENERATA: ", complex)
        // return complex;
        return `${key}(${Array(args).join(',')})`;
    }catch (err) {
        console.error(`Errore in _keyArgsString: ${err.name} - ${err.message}`)
    }
}



module.exports = node_cache_CachedClass;