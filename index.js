const node_cache_CachedClass = require ('./lib/node-cache_CachedClass');
const {custErrors} = require('leukos-tech-jsutils');

/**
 * 
 * 
 * Restituisce una istanza della classe `CachedClass` basata sull'engine `engine`.
 * 
 * @factory
 * 
 * @param {String} engine Nome dell'engine per la cache da utilizzare (def. `node-cache`)
 * 
 * @return {Object} Istanza di `CachedClass`
 * 
 * @throws {WrongArgTypeError} Se la stringa fornita come argomento `engine` non corrisponde ad un engine supportato. 
 */
function _factory(engine="node-cache") {

    switch (String(engine).toLowerCase()) {
        case "node-cache":
            return new node_cache_CachedClass();
        default:
            throw new custErrors.args.WrongArgTypeError("engine", engine, ['node-cache']);
    }
}
module.exports = _factory;