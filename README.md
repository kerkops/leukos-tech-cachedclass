# Super-Classe da ereditare per conferire ad una determinata classe funzionalità di *caching*

Una volta ereditata o utilizzata per valorizzare un attributo della classe contenitore espone un set di metodi **SINCRONI** che consentono le funzionalità di *caching*:

* `.cacheSet(key, value)` -> Memorizza un valore 
* `.cacheGet(key)` -> Recupera un valore memorizzato (se disponibile) 
* `.cacheDel(key)` -> Elimina un valore memorizzato 
* `.cacheFlush()` -> Azzera la cache, eliminando tutti i valori memorizzati 

Per ciascuno dei metodi sopra è disponibile il corrispettivo metodo di convenienza **`.####_args()`**, che consente di gestire il risultato di una chiamata ad un metodo in combinazione agli argomenti utilizzati.

* `.cacheSet_args(methodName, args, value)`
* `.cacheGet_args(methodName, args)` 
* `.cacheDel_args(methodName, args)` 

## Tipi supportati

Attualmente i tipi di dato memorizzabili nella cache sono:
* **String**
* **Number**
* **Boolean**
* **Array**
* **Object** (non autoreferenziato e privo di metodi, ie. *json object*)

## Installazione

`npm install leukos-tech-cachedclass --save`

## Utilizzo 

### L'import restituisce una factory

 `const cacheFactory = require('leukos-tech-cachedclass);`

### Attraverso la factory si ottiene la CachedClass (è necessario specificare il nome dell'engine - default `node-cache`)

`const cache = cacheFactory("node-cache");`

 `...`

### Esempio di utilizzo per ereditarietà 

 `class LaMiaClasse extends cache {`

> `constructor() {`
>>
>> `super();`
>>
> `};`
>
> // La cache viene applicata alla chiamata del metodo `metodoCached()`
>
> `metodoCached(arg1, arg2, arg3) {`
>>
>> // Per prima cosa si tenta di recuperare il valore relativo alla combinazione *nome del metodo* + *argomenti*
>>
>>  **`let value = this.cacheGet_args("metodoCached", [arg1, arg2, arg3]);`**
>>
>>
>> // La funzione restituisce `undefined` se la chiave non esiste nella cache. Se restituisce un valore diverso, il valore è valido e viene immediatamente restituito
>>
>> **`if (value != undefined) { return value };`**
>>
>> // Se non è stata eseguita la funzione di `return`, avviene il regolare svolgimento del metodo, che produce il valore da restituire.
>>
>> *` .... long operations .... `*
>> 
>> `value = "UnQualcheValore;`
>>
>> // Una volta ottenuto il valore, prima di restituirlo, viene memorizzato nella *cache*. Il metodo restituisce `false` se il tentativo di memorizzare il valore nella cache è fallito
>>
>> `if ( ` **` !this.cacheSet_args("metodoCached", [arg1, arg2, arg3], value)`** ` ) {`
>>
>>> `console.log("Impossibile memorizzare il valore nella cache");`
>>
>> `};`
>>
>> `return value;`
>>
>>
> `};`
`};`

---

---

# Metodi Pubblici (Interfaccia)

## `CachedClass.cacheDel(key {String}): deleted {Boolean}`

Elimina un elemento dalla *cache*

### Argomenti

* `key` **{String}** Chiave associata all'elemento da eliminare.

### Tipi Restituiti

* `deleted` **{Boolean}** `true` se l'elemento è stato eliminato. `false` se non è stata possibile l'eliminazione. o se la chiave non è valida.

---

## `CachedClass.cacheDel_args( methodName {String}, args {Array} ): deleted {Boolean}`

Elimina il valore, memorizzato nella cache, prodotto dalla chiamata al metodo `methodName` con gli argomenti `args`.

### Argomenti

* `methodName` **{String}** Nome del metodo che ha generato il valore da eliminare.

* `args` **{Array}** Argomenti utilizzati per la chiamata al metodo che ha prodotto il valore da eliminare.

### Tipi Restituiti

* `deleted` **{Boolean}** `true` se l'elemento è stato eliminato. `false` se non è stata possibile l'eliminazione. o se la chiave non è valida.

---

## `CachedClass.cacheFlush(): Null`

Azzera il contenuto della cache.

---

## `CachedClass.cacheGet( key {String} ): value {Any|undefined}`

Restituisce un valore memorizzato nella *cache* (se presente).

### Argomenti

* `key` **{String}** Chiave associata al valore da ottenere.

### Tipi restituiti

* `value` **{Any}** Valore memorizzato nella *cache* alla chiave `key` 

**Se la chiave non esiste il metodo restituisce *`undefined`***.

---

## `CachedClass.cacheGet_args( methodName {String}, args {Array} ): value {Any|undefined}`

Restituisce il valore memorizzato nella cache della chiamata a `methodName` con gli argomenti `args`.
Se nessun elemento della cache corrisponde a tale combinazione restituisce *`undefined`*.

### Argomenti

* `methodName` **{String}** Nome del metodo di cui recuperare il valore memorizzato

* `args` **{Array}** Argomenti per la chiamata al metodo il cui tipo restituito è salvato nella cache

### Tipi restituiti

* `value` **{Any}** Valore associato alla combinazione di `methodName` e `args`. 

**Se nessun valore è associato il metodo restituisce *`undefined`***.

---

## `CachedClass.cacheSet( key {String}, value {Any} ): inserted {Boolean}`

Memorizza il valore `value` sotto la chiave `key`.

### Argomenti

* `key` **{String}** Chiave da utilizzare per identificare il valore nella cache.

* `value` **{Any}** value Valore da memorizzare. Se uguale a *`undefined`* il metodo restiuisce `false` e non lo memorizza.

### Tipi restituiti

* `inserted` **{Boolean}** `true` se il valore è stato memorizzato.

---

## `CachedClass.cacheSet( key {String}, value {Any} ): inserted {Boolean}`

Memorizza il valore `value` restituito dalla chiamata al metodo `methodName` con gli argomenti `args`.

### Argomenti

* `methodName` **{String}** Nome del metodo di cui si vuol salvare il valore restituito.

* `args` **{Array}** Array di argomenti utilizzati per la chiamata al metodo.

* `value` **{Any}** Valore da memorizzare. Deve essere diverso da . Se uguale a *`undefined`*, il metodo restiuisce `false` e non memorizza il valore.

### Tipi restituiti

* `inserted` **{Boolean}** `true` se il valore è stato memorizzato.

---

